package View;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

public class Show extends JFrame{
	private JComboBox<Object> box;
	private JButton ok;
	private JLabel enter_n;
	private JLabel choose;
	private JTextField loop_n;
	private JScrollPane scroll;
	private JTextArea text;
	private JPanel panelText;
	private JPanel panelComboBox;
	private JPanel panel1;
	private JPanel panel2;
	private JPanel panel3;
	
	
	private static final int DEFAULT_N = 0;
	
	public Show(){
		createPanel();
	}
	
	public JPanel createComboBox(){
		box = new JComboBox<>();
		box.addItem("choose nested loop");
		box.addItem("Nested Loop 1");
		box.addItem("Nested Loop 2");
		box.addItem("Nested Loop 3");
		box.addItem("Nested Loop 4");
		box.addItem("Nested Loop 5");
		
		JPanel panel = new JPanel();
		panel.add(box);
		return panel;
	}
	
	public void createPanel(){
		text = new JTextArea();
		scroll = new JScrollPane(text);
		JPanel comboBox = createComboBox();
		enter_n = new JLabel("Enter n loops:");
		choose = new JLabel("Choose nested loop");
		ok = new JButton("Ok");
		
		final int FIELD_WIDTH = 10;
		loop_n = new JTextField(FIELD_WIDTH);
		loop_n.setText(""+DEFAULT_N);
		
		panel1 = new JPanel();
		panel1.add(enter_n);
		panel1.add(loop_n);
		panel2 = new JPanel();
		panel2.add(choose);
		panel2.add(comboBox);
		panel3 = new JPanel();
		panel3.add(ok);
		
		panelComboBox = new JPanel();
		panelComboBox.setLayout(new GridLayout(3,1));
		panelComboBox.add(panel1);
		panelComboBox.add(panel2);
		panelComboBox.add(panel3);
		
		panelText = new JPanel();
		panelText.setLayout(new GridLayout(2,1));
		panelText.add(scroll);
		panelText.add(panelComboBox);
		
		add(panelText,BorderLayout.CENTER);
		
	}
	
	public void setListener(ActionListener list){
		ok.addActionListener(list);
	}
	
	public String getCombo(){
		return (String) box.getSelectedItem();
	}
	
	public void setResult(String s){
		text.setText(s);
	}
	
	public int getN(){
		int n = Integer.parseInt(loop_n.getText());
		return n;
	}
}
