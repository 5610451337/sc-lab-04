package model;


public class Nested_Loop {
	public String loop1(int a){
		String str = "";
		for(int i=1;i<=a;i++){
			for(int j=1;j<=4;j++){
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	public String loop2(int a){
		String str = "";
		for(int i=1;i<=a;i++){
			for(int j=1;j<=3;j++){
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	public String loop3(int a){
		String str = "";
		for(int i=1;i<=a;i++){
			for(int j=1;j<=i;j++){
				str += "*";
			}
			str += "\n";
		}
		return str;
	}
	public String loop4(int a){
		String str = "";
		for(int i=1;i<=a;i++){
			for(int j=1;j<=5;j++){
				if(j%2 ==0){
					str += "*";
				}
				else {
					str += "-";
				}
			}
			str += "\n";
		}
		return str;
	}
		public String loop5(int a){
			String str = "";
			for(int i=1;i<=a;i++){
				for(int j=1;j<=4;j++){
					if((i+j)%2 ==0){
						str += "*";
					}
					else {
						str += " ";
					}
				}
				str += "\n";
			}
			return str;
		}
}
