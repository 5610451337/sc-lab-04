package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Nested_Loop;
import View.Show;

public class Control {
	private Nested_Loop nested;
	private Show frame;
	private ActionListener button;
	
	public static void main(String[] args){
		new Control();
	}
	
	public Control(){
		nested = new Nested_Loop();
		frame = new Show();
		frame.setVisible(true);
		frame.setSize(300,300);
		frame.setLocation(550,150);
		button = new AddComboListener();
		frame.setListener(button);
	}
	
	class AddComboListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if(frame.getCombo()=="Nested Loop 1"){
				frame.setResult(nested.loop1(frame.getN()));
			}
			else if(frame.getCombo()=="Nested Loop 2"){
				frame.setResult(nested.loop2(frame.getN()));
			}
			else if(frame.getCombo()=="Nested Loop 3"){
				frame.setResult(nested.loop3(frame.getN()));
			}
			else if(frame.getCombo()=="Nested Loop 4"){
				frame.setResult(nested.loop4(frame.getN()));
			}
			else if(frame.getCombo()=="Nested Loop 5"){
				frame.setResult(nested.loop5(frame.getN()));
			}
			else {
				frame.setResult("Choose again");
			}
		}
	}
}
